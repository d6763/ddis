# DDIS

This is the diploma project of two students from the Kazakh-British Technical University.

## Name
DDIS - Decentralized Docker Image Storage

## Description
We all know the dependence of the IT block on external factors, one of the examples is politics. In our diploma project, we analyzed this situation and came to the conclusion about the transition of an independent decentralized flow of technology development into IT. The object of study became Docker with a centralized repository Docker Hub

## Visuals
[Demo web-site][web-site]

[web-site]: https://ddis.sytes.net

We tested without internet by connecting peers locally

![Test local peers](/src/gif/test-local.gif)

## Prerequisites
You need Docker and docker-compose to launch this app. But every developer has it on their computer)

## Installation

Just one command and your environment is ready. No installation, no compiling.

```
docker-compose up
```

To up as the daemon

```
docker-compose up -d 
```


## Authors and acknowledgment
Zhuman Rakhat and Tolegenov Bekzat

## Project status
Actively developing
